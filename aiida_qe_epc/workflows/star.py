# -*- coding: utf-8 -*-
import os, numpy
from aiida import orm
from aiida.common import AttributeDict
from aiida.engine import WorkChain, ToContext, while_, append_

from aiida.plugins import CalculationFactory
EPCCalculation = CalculationFactory('qe_epc')
PhCalculation = CalculationFactory('quantumespresso.ph')
def default_parameters_dict():
    return {'INPUTPH': {'alpha_mix': 0.3, 'tr2_ph': 1e-18}}

def default_options_dict():
    return {'resources':{"num_machines": 1},
             'max_wallclock_seconds':60*60*4,
             }

class EPCStarWorkChain(WorkChain):
    """
    """
    @classmethod
    def define(cls, spec):
        super(EPCStarWorkChain, cls).define(spec)
        spec.input('code', valid_type=orm.Code)
        spec.input('parent_calculation', valid_type = PhCalculation, required=False)
        spec.input('parent_folder', valid_type=(orm.FolderData, orm.RemoteData), required=False)
        spec.input('dynfile', valid_type = orm.SinglefileData, required=False)
        spec.input('kpoints', valid_type = orm.KpointsData)
        spec.input('qpoint', valid_type = orm.KpointsData)
        spec.input('targetqpoints', valid_type = orm.KpointsData, required=False)
        spec.input('dvscf', valid_type=(orm.SinglefileData, orm.RemoteData), required=False)
        spec.input('dvscf_paw', valid_type=(orm.SinglefileData, orm.RemoteData),required=False)
        spec.input('parameters', valid_type=orm.Dict, required=False)
        spec.input('settings', valid_type=orm.Dict, required=False)
        spec.input('options', valid_type=orm.Dict, required=False)
        spec.outline(
            cls.validate_inputs,
            cls.setup_star,
            cls.run_star,
            cls.collect_star,
            while_(cls.qstar_to_run) (
                cls.setup,
                while_(cls.try_again)(
                    cls.run_epc,
                    cls.inspect_epc,
                ),
            ),
            cls.run_results,
        )
        spec.outputs.dynamic = True
        spec.outputs.valid_type = orm.Data

    def validate_inputs(self):
        """
        An EPCCalculation follows a single q point PhCalculation in which case
        the parent_calculation input should be set, or it can be a restart from a previous EPCCalculation
        , in which case the parent_folder should be set. In either case, at least one
        of the two inputs has to be defined properly
        """
        if not ('parent_calculation' in self.inputs or 'parent_folder' in self.inputs):
            self.abort_nowait('Neither the parent_calculation nor the parent_folder input was defined')

        try:
            parent_folder = self.inputs.parent_calculation.outputs.remote_folder
        except AttributeError:
            parent_folder = self.inputs.parent_folder

        self.ctx.parent_folder = parent_folder

        qpoint = self.inputs.qpoint
        cell = qpoint.cell
        alat = numpy.linalg.norm(cell[0])
        self.ctx.twopioa = 2*numpy.pi/alat
        self.ctx.cell = cell

        if 'targetqpoints' in self.inputs:
            # in twopioa...
            self.ctx.targetqpoints = self.inputs.targetqpoints.get_kpoints(cartesian=True) /  self.ctx.twopioa
            self.ctx.notarget = False
        else :
            self.ctx.targetqpoints = []
            self.ctx.notarget = True
        if 'parameters' in self.inputs:
            self.ctx.parameters = self.inputs.parameters.get_dict()
        else:
            self.ctx.parameters = default_parameters_dict()

        if 'options' in self.inputs:
            self.ctx.options = self.inputs.options.get_dict()
        else:
            self.ctx.options = default_options_dict()





    def setup_star(self):
        """
        conpute all the star quantities
        """
        self.ctx.is_finished = False

        # Define the parameters to compute the star q's and dvscf's
        params = self.ctx.parameters
        params['INPUTPH']["dvscf_star%open"]=True
        params['INPUTPH']["dvscf_star%basis"]='cartesian'
        dumks = orm.KpointsData()
        dumks.set_cell(self.inputs.kpoints.cell)
        dumks.set_kpoints([[0.1, 0.1, 0.0]])

        # Define dictionary of inputs for EPCCalculation
        self.ctx.inputs = {
            'code': self.inputs.code,
            'kpoints': dumks,
            #'dynfile': self.inputs.dynfile,
            'qpoint': self.inputs.qpoint,
            'parameters': orm.Dict(dict=params),
            'parent_folder': self.inputs.parent_folder,
            #'dvscf': self.inputs.dvscf,
            'settings': orm.Dict(dict={'rotate_dvscf':True}),
            'metadata': {
                'options': self.ctx.options
            }
        }
        if "dvscf" in self.inputs:
            self.ctx.inputs["dvscf"] = self.inputs.dvscf
        if "dynfile" in self.inputs:
            self.ctx.inputs["dynfile"] = self.inputs.dynfile

        return

    def run_star(self):
        """
        Run the EPCCalculation
        """
        running = self.submit(EPCCalculation, **self.ctx.inputs)

        self.report('launching EPCCalculation<{}> star run'.format(running.pk))

        return ToContext(starcalculation=running)

    def collect_star(self):
        """
        process the star, that is, collect the q's and dvscf's
        """
        calc=self.ctx.starcalculation
        try:
            rot_dvscf = calc.outputs.Rotated_DVSCF.get_dict()
            self.ctx.nostar = False
        except Exception:
            self.report('The star calculation did not yield any Rotated_DVSCF folder')
            self.ctx.iteration = 0
            self.ctx.max_iterations = 0
            self.ctx.nostar = True
            return

        if self.ctx.notarget:
            qstars = rot_dvscf['q_stars']
            dvscffiles = rot_dvscf['dvscf_files']
        else:
            qstars = []
            dvscffiles = []
            for qstar, dvscff in zip(rot_dvscf['q_stars'],rot_dvscf['dvscf_files']):
                if min([numpy.linalg.norm(numpy.array(qstar)-x) for x in self.ctx.targetqpoints])<1e-1:
                    qstars.append(qstar)
                    dvscffiles.append(dvscff)
        self.ctx.q_stars=qstars
        self.ctx.rot_folder= calc.outputs.remote_folder.uuid#retrieved.uuid #rot_dvscf['folder']
        self.ctx.dvscf_files=dvscffiles
        self.ctx.iteration = 0
        self.ctx.max_iterations = len(self.ctx.q_stars)
        return

    def qstar_to_run(self):
        return ( self.ctx.iteration < self.ctx.max_iterations )


    def setup(self):
        """
        Initialize context variables
        """
        self.ctx.is_finished = False

        qstarpoint = orm.KpointsData()
        # this one is in twopioa
        xqdum = self.ctx.q_stars.pop()
        qstar = numpy.array(xqdum)*self.ctx.twopioa
        qstarpoint.set_cell(self.ctx.cell)
        qstarpoint.set_kpoints([qstar], cartesian = True)
        dvscffilename = self.ctx.dvscf_files.pop()
        #
        params=self.ctx.parameters
        params['INPUTPH']["dvscf_star%open"]=False
        params['INPUTPH']["dvscf_star%basis"]='cartesian'
        # Define convenience dictionary of inputs for EPCCalculation
        self.ctx.inputs = {
            'code': self.inputs.code,
            'kpoints': self.inputs.kpoints,
            'qpoint': qstarpoint,
            'parameters': orm.Dict(dict=params),
            'parent_folder': self.inputs.parent_folder,
            'settings': orm.Dict(dict={'replace_dvscf':(self.ctx.rot_folder,dvscffilename)}),
            'metadata': {
                'options': self.ctx.options,
            }
        }
        if "dynfile" in self.inputs:
            self.ctx.inputs["dynfile"] = self.inputs.dynfile

        self.ctx.already_tried = False
        return

    def try_again(self):
        return (not self.ctx.is_finished)

    def run_epc(self):
        """
        Run the EPCCalculation
        """
        running = self.submit(EPCCalculation, **self.ctx.inputs)

        self.report('launching EPCCalculation<{}> qstar #{}'.format(running.pk, self.ctx.iteration))

        return ToContext(calculations=append_(running))

    def inspect_epc(self):
        """
        Analyse the results of the previous EPCCalculation, checking whether it finished successfully
        or if not troubleshoot the cause and adapt the input parameters accordingly before
        restarting, or abort if unrecoverable error was found
        """
        try:
            calculation = self.ctx.calculations[-1]
        except Exception:
            self.report('the first iteration finished without returning a EPCCalculation')
            if self.ctx.already_tried:
                self.ctx.is_finished = True
                self.ctx.iteration += 1
            else:
                self.ctx.already_tried = True
            return

        # Done: successful convergence of last calculation
        if calculation.is_finished_ok:
            self.report('EPC computed successfully.')
            self.ctx.parent_folder = calculation.outputs.remote_folder
            self.ctx.is_finished = True
            self.ctx.iteration += 1
        elif self.ctx.already_tried:
            self.report('submission for EPCCalculation<{}> failed, giving up'.format(
              calculation.pk))
            self.ctx.is_finished = True
            self.ctx.iteration += 1
        # Retry: submission failed, try to restart or abort
        else:
            self.report('submission for EPCCalculation<{}> failed, re_launching'.format(
                calculation.pk))
            self.ctx.already_tried = True

        return

    def run_results(self):
        """
        Attach the output parameters and retrieved folder of all the star calculations
        to the outputs
        """
        if self.ctx.nostar:
            self.report("star didn;t work, giving up")
            return

        self.ctx.starcalculation.outputs.remote_folder._clean()
        # The remote folders contains the
        # dvscfs, which will fill up your disk in no time.
        result={}
        for i,calculation in enumerate(self.ctx.calculations):
            self.out('elphmat_'+str(i), calculation.outputs.elphmat)
            calculation.outputs.remote_folder._clean()
        self.report('workchain completed after {} iterations'.format(self.ctx.iteration))
