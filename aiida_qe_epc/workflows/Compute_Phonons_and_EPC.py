# -*- coding: utf-8 -*-
import aiida
import numpy as np
import spglib as sp
import re
import os, ase
from aiida import orm
from aiida.common import AttributeDict
from aiida.engine import WorkChain, ToContext, if_, append_, submit
from aiida.plugins import WorkflowFactory, CalculationFactory
from aiida.orm.utils import load_node
from aiida_quantumespresso.utils.mapping import prepare_process_inputs
PwBaseWorkChain = WorkflowFactory('quantumespresso.pw.base')
PhBaseWorkChain = WorkflowFactory('quantumespresso.ph.base')
EPCStarWorkChain = WorkflowFactory('qe_epc.star')
EPCCalculation = CalculationFactory('qe_epc')

Atobohr = 1.8897259850078587


def default_options_dict():
    return {'resources':{"num_machines": 1},
             'max_wallclock_seconds':60*60*4,
             }


class Compute_Phonons_and_EPC(WorkChain):
    """
    """
    @classmethod
    def define(cls, spec):
        super(Compute_Phonons_and_EPC, cls).define(spec)
        spec.input('code_pw', valid_type=orm.Code)
        spec.input('code_ph', valid_type=orm.Code)
        spec.input('scf_remote', valid_type = orm.RemoteData)
        spec.input('init_ks', valid_type = orm.KpointsData)
        spec.input('qpoints', valid_type = orm.KpointsData, required = False)
        spec.input('target_qs', valid_type = orm.KpointsData, required = False)
        #spec.input('irr_qs', valid_type = orm.KpointsData, required = False)
        spec.input('parameters', valid_type = orm.Dict)
        spec.input('options_pw', valid_type = orm.Dict, required=False)
        spec.input('options_ph', valid_type = orm.Dict, required=False)
        spec.input('parameters_ph', valid_type = orm.Dict, required=False)
        spec.outline(
            cls.validate_inputs,
            cls.run_scf,
            cls.prepare_ph_runs,
            if_(cls.ph_to_run)(cls.run_ph),
            cls.run_epc,
            cls.run_results
        )
        spec.outputs.dynamic = True
        spec.outputs.valid_type = orm.Data

    def validate_inputs(self):
        self.ctx.inputs = AttributeDict({
            'code_pw': self.inputs.code_pw,
            'code_ph': self.inputs.code_ph,
            'scf_remote' : self.inputs.scf_remote,
            'init_ks' : self.inputs.init_ks,
            'parameters' : self.inputs.parameters
        })
        #
        #self.ctx.inputs.parameters=self.inputs.parameters
        #check content
        #
        if 'options_pw' in self.inputs:
            self.ctx.inputs.options_pw = self.inputs.options_pw.get_dict()
        else:
            self.ctx.inputs.options_pw = default_options_dict()
        #
        if 'options_ph' in self.inputs:
            self.ctx.inputs.options_ph = self.inputs.options_ph.get_dict()
        else:
            d = self.ctx.inputs.options_pw
            d['resources']["num_machines"] = d['resources']["num_machines"] * 1
            d['max_wallclock_seconds'] = d['max_wallclock_seconds'] * 4
            self.ctx.inputs.options_ph = d
        #
        # basic parameters taken from Davide's phonon calculations
        if 'parameters_ph' in self.inputs:
            self.ctx.ph_parameters = self.inputs.parameters_ph
        else:
            self.ctx.ph_parameters = orm.Dict(dict={'INPUTPH':{'recover':True,'fildvscf':'dvscf',
                                                    'alpha_mix(1)': 0.4, 'tr2_ph': 1e-18 }})
        #
        #self.ctx.inputs.irr_qs = self.inputs.irr_qs
        # alat in angstrom
        alat=np.linalg.norm(self.ctx.inputs.scf_remote.creator.inputs.structure.get_ase().get_cell()[0])
        self.ctx.twopioa=2*np.pi/alat
        #
        # nosym flag
        if 'nosym' in  self.ctx.inputs.parameters.keys():
            self.ctx.nosym = self.inputs.parameters['nosym']
        else:
            self.ctx.nosym = False
            self.report('inputs validated')

        self.ctx.phtorun = False
        if 'qpoints' in self.inputs and not 'restart' in  self.ctx.inputs.parameters.keys():
            self.ctx.qs_to_compute = self.inputs.qpoints.get_kpoints(cartesian=True)
            self.report('<{}> q-points to compute'.format(len(self.ctx.qs_to_compute)))
            #
            self.ctx.phtorun = True
        #


    def run_scf(self):
        #
        # check if template scf was done on the same code as the one specified in input
        # and if the remote folder is still there, in that case we don't have to re-run it
        scf = self.ctx.inputs.scf_remote.creator
        # no need for a scf calculation...
        if not self.ctx.phtorun:
            return
        same_code = scf.computer.uuid == self.ctx.inputs.code_pw.computer.uuid
        try:
            RF_still_here = not scf.outputs.remote_folder.is_empty
        except:
            self.report('Failed to locate template scf, re-running scf')
            RF_still_here = False
        #
        if same_code and RF_still_here:
            self.ctx.scf = scf
            self.report('Using remote_scf as starting point ')
        else:
            self.report('Re-running scf to start ')
            #
            b = PwBaseWorkChain.get_builder()
            b.pw.code = self.ctx.inputs.code_pw
            b.pw.structure = scf.inputs.structure
            b.kpoints =  scf.inputs.kpoints
            #
            pseudos= [a.node for a in scf.get_incoming(link_label_filter="pseudo%")]
            ps_dict = {p.attributes['element']:p for p in pseudos}
            b.pw.pseudos = ps_dict
            params = scf.inputs.parameters.get_dict()
            if 'wf_collect' in params['CONTROL'].keys() and params['CONTROL']['wf_collect']:
                b.pw.parameters = scf.inputs.parameters
            else:
                params['CONTROL']['wf_collect'] = True
                b.pw.parameters = orm.Dict(dict= params)
            b.pw.metadata.options = self.ctx.inputs.options_pw
            #
            proc=self.submit(b)
            self.report('launching base PwCalculation<{}>'.format(proc.pk))
            #
            return ToContext(scf = proc)

    def prepare_ph_runs(self):
        #
        params = self.ctx.inputs.parameters.get_dict()
        if 'npools_ph' in params.keys():
            self.ctx.npools = params['npools_ph']
        else:
            self.ctx.npools = 1
        #
        if 'from_ph_calcs' in params.keys() and params['from_ph_calcs'] != []:
            self.ctx.ph_calculations = []
            for ph_uu in params['from_ph_calcs']:
                self.ctx.ph_calculations.append(load_node(ph_uu))
                self.report('recovered ph calculations{}'.format(ph_uu))

    def ph_to_run(self):
        return self.ctx.phtorun

    def run_ph(self):
        #
        try:
            structure = self.ctx.scf.inputs.structure
        except:
            structure = self.ctx.scf.inputs.pw__structure
        for i,q in enumerate(self.ctx.qs_to_compute):
            qpoints = orm.KpointsData()
            qpoints.set_cell_from_structure(structure)
            qpoints.set_kpoints([q], cartesian=True)
            #
            # Retrieve the dvscf, if we want to relaunch the calculations
            settings = {'cmdline':['-nk',str(self.ctx.npools)],
                        'ADDITIONAL_RETRIEVE_LIST' : [os.path.join('out', '_ph0', 'aiida.dvscf1')]}
            #
            b = PhBaseWorkChain.get_builder()
            b.ph.code = self.ctx.inputs.code_ph
            b.ph.metadata.options = self.ctx.inputs.options_ph
            b.ph.parent_folder = self.ctx.scf.outputs.remote_folder
            b.ph.parameters = self.ctx.ph_parameters
            b.ph.qpoints = qpoints
            b.ph.settings = orm.Dict(dict = settings)
            #
            future = self.submit(b)
            self.report('launching PhCalculation<{}>'.format(future.pk))
            self.to_context(ph_calculations=append_(future))

    def run_epc(self):
        for i, ph_calc in enumerate(self.ctx.ph_calculations):
            try:
                RF=ph_calc.outputs.remote_folder
            except:
                self.report('problem with ph iteration<{}>, skipping it'.format(i))
                continue
            # to test presence of the necessary data
            ph0 = RF.listdir("out/_ph0")
            rdvscf = re.compile("aiida.dvscf\d+")
            DM = RF.listdir("DYN_MAT")
            rdyn = re.compile("dynamical-matrix-.*")
            #
            if any([rdvscf.match(x) for x in ph0]) and any([rdyn.match(x) for x in DM]):
                # go up to the calculation, not the base workflow
                phc = RF.creator
                epc_inputs={
                    'code':self.inputs.code_ph,
                    'parent_folder':RF,
                    'kpoints':self.ctx.inputs.init_ks,
                    'qpoint': phc.inputs.qpoints,
                    }
                if 'target_qs' in self.inputs and not self.ctx.nosym:
                    epc_inputs['targetqpoints'] = self.inputs.target_qs
                #
                params = self.ctx.inputs.parameters.get_dict()
                if 'ibmin' in params.keys() and 'ibmax' in params.keys():
                    epc_inputs['parameters'] = orm.Dict(dict={
                    'INPUTPH': {'alpha_mix': 0.3, 'tr2_ph': 1e-18,
                    'ibmin':params['ibmin'], 'ibmax':params['ibmax']}})
                if self.ctx.nosym:
                    epc_inputs['metadata']  = {'options': {'resources':{"num_machines": 1},
                                 'max_wallclock_seconds':60*60*1}}
                    future=self.submit(EPCCalculation, **epc_inputs)
                    self.report('launching EPCalculation<{}>'.format(future.pk))
                else:
                    future=self.submit(EPCStarWorkChain, **epc_inputs)
                    self.report('launching EPCStarWorkChain<{}>'.format(future.pk))
                self.to_context(epc_calculations = append_(future))
            else:
                self.report('problem with ph iteration<{}>, skipping it'.format(i))
                self.ctx.iteration_epc += 1

    def run_results(self):
        #
        results={}
        j = 0
        for i, epc_calc in enumerate(self.ctx.epc_calculations):
            try:
                for a in epc_calc.get_outgoing(link_label_filter='elphmat%'):
                    self.out('elphmat'+str(j), a.node)
                    j+=1
            except:
                self.report('problem with ph iteration<{}>, skipping it'.format(i))
        #
        self.report('workchain completed')
