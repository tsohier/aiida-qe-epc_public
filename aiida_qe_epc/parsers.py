# -*- coding: utf-8 -*-

import glob, os, re
from aiida import orm
from aiida.common import exceptions
from aiida.parsers.parser import Parser
from aiida_quantumespresso.parsers import QEOutputParsingError, get_parser_info

from aiida.plugins import CalculationFactory
EPCCalculation = CalculationFactory('qe_epc')

import xmltodict
import itertools
import numpy as np

RytoeV = 13.605698

def read_diag_calc_dynmat(dynmat, targetq, xml=False):
    if xml:
        metoRy=911.1477777956469
        data = xmltodict.parse(dynmat)['Root']
        info=data['GEOMETRY_INFO']
        ntyp = int(info['NUMBER_OF_TYPES']['#text'])
        nat = int(info['NUMBER_OF_ATOMS']['#text'])
        ibrav=int(info['BRAVAIS_LATTICE_INDEX']['#text'])
        masses={}
        for i in range(ntyp):
            masses[info['TYPE_NAME.'+str(i+1)]['#text'].strip()]=float(info['MASS.'+str(i+1)]['#text'])*metoRy
        atorder=[ info['ATOM.'+str(i+1)]['@SPECIES'].strip() for i in range(nat) ]
        nqs=float(info['NUMBER_OF_Q']['#text'])
        dynkeys=[key for key in data.keys() if 'DYNAMICAL' in key]
        dum=[data[key] for key in dynkeys if np.linalg.norm(targetq-np.array([float(x) for x in data[key]['Q_POINT']['#text'].split()]))<1e-5]
        dynmat=dum[0]
        D=np.zeros((3*nat, 3*nat), dtype=complex)
        for a, b in itertools.product(list(range(nat)),list(range(nat))):
            dd=np.array([ float(l.split(',')[0].strip())+1j*float(l.split(',')[1].strip())  for l in dynmat['PHI.'+str(a+1)+'.'+str(b+1)]['#text'].splitlines()])
            for i, j in itertools.product(list(range(3)),list(range(3))):
                D[3*a+i,3*b+j]=dd[3*i+j]/np.sqrt(masses[atorder[a%3]]*masses[atorder[b%3]])
        w2, v = np.linalg.eigh(D)
        vnus=[v[:,nu] for nu in range(3*nat)]
        zznus=[[vnu[i]/np.sqrt(masses[atorder[int(i/3)]]) for i in range(3*nat)] for vnu in vnus]
    else:
        lines = dynmat.splitlines()
        ntyp = int(lines[2].split()[0])
        nat = int(lines[2].split()[1])
        ibrav=int(lines[2].split()[2])
        if ibrav==0:
            iline=7
        else:
            iline=3
        masses=[float(lines[iline+i].split()[3]) for i in range(ntyp)]
        iline=iline+ntyp
        mmat=[masses[int(lines[iline+i].split()[1])-1] for i in range(nat)]
        for nline,line in enumerate(lines):
            if 'q =' in line:
                q=[float(x) for x in re.findall('[- ][0-9].[0-9]*',line)]
                if np.linalg.norm(np.array(targetq)-np.array(q))<1e-5:
                    break
        matfull=False
        iline=nline+2
        #D[a,i][a',j]
        D=np.empty([3*nat,3*nat], dtype=complex)
        while matfull==False:
            a,b=[int(x)-1 for x in lines[iline].split()]
            iline += 1
            for i in [0,1,2]:
                for j in [0,1,2]:
                    Re,Im=[float(x) for x in lines[iline].split()[2*j:2*j+2]]
                    D[3*a+i][3*b+j]=np.complex(Re,Im)/np.sqrt(mmat[a]*mmat[b])
                iline += 1
            if a == nat-1 and b == nat -1 : matfull=True
        w2, v = np.linalg.eigh(D)
        vnus=[v[:,nu] for nu in range(3*nat)]
        zznus=[[vnu[i]/np.sqrt(mmat[int(i/3)]) for i in range(3*nat)] for vnu in vnus]
    return [D,w2,zznus]

def proj_elph(w2s, zzlist, elphmat):
    """
    Project the el-ph matrix in atomic/Cartesian onto the phonon modes
    """
    elph = np.array(elphmat)
    zzvecs = [np.array(z) for z in zzlist]
    g2s = np.real([
        np.dot(np.dot(z.conjugate(), elph), z) * RytoeV**2 / (2 * np.sqrt(w2))
        for z, w2 in zip(zzvecs, w2s)
    ])
    return g2s

class EPCParser(Parser):
    """
    Parser implementation for Quantum ESPRESSO EPC calculations
    """

    def parse(self, retrieved_temporary_folder=None, **kwargs):
        """
        Parse the results of retrieved nodes
        """
        try:
            output_folder = self.retrieved
        except exceptions.NotExistent:
            return self.exit_codes.ERROR_NO_RETRIEVED_FOLDER

        list_of_files = output_folder.list_object_names()

        # The stdout is required for parsing
        filename_stdout = EPCCalculation._OUTPUT_FILE_NAME
        filename_elphmat = EPCCalculation._OUTPUT_ELPHMAT_NAME
        filepath_dynmat = EPCCalculation._FOLDER_DYNAMICAL_MATRIX
        filepath_rot_dvscf = EPCCalculation._PREFIX+"."+EPCCalculation._INPUT_DVSCF_SUFFIX+".dfile_dir"
        #EPCCalculation._OUTPUT_STAR_FOLDER_NAME

        if filename_stdout not in list_of_files:
            self.logger.error("The standard output file '{}' was not found but is required".format(filename_stdout))
            return self.exit_codes.ERROR_READING_OUTPUT_FILE

        out_file = output_folder._repository._get_base_folder().get_abs_path(filename_stdout)
        is_success, dict_stdout = self.parse_stdout(out_file)
        self.out('output_parameters', orm.Dict(dict=dict_stdout))

        complete_calculation = True

        filepath_elphmat = output_folder._repository._get_base_folder().get_abs_path(filename_elphmat)

        if not os.path.isfile(filepath_elphmat):
            complete_calculation = False
            self.logger.info("output file '{}' was not found, assuming partial calculation".format(filepath_elphmat))
            return self.exit_codes.ERROR_READING_ELPHMAT_FILE

        if complete_calculation:
            out_dict = self.parse_elphmat(filepath_elphmat)
            elphmatarray = orm.ArrayData()
            for name, ar in out_dict.items():
                elphmatarray.set_array(name, ar)
            self.out('elphmat', elphmatarray)

        rot_dvscf_dir = output_folder._repository._get_base_folder().get_abs_path(filepath_rot_dvscf)
        if os.path.isfile(rot_dvscf_dir):
            rot_dvscfdict = self.parse_rotated_dvscf(rot_dvscf_dir)
            rot_dvscf = orm.Dict(dict=rot_dvscfdict)
            self.out('Rotated_DVSCF', rot_dvscf)

        return

    def parse_stdout(self, filepath):
        """
        Parse the output parameters from the output of a EPC calculation
        written to standard out

        :param filepath: path to file containing output written to stdout
        :returns: boolean representing success status of parsing, True equals parsing was successful
        :returns: dictionary with the parsed parameters
        """
        is_successful = True
        is_terminated = True
        result = get_parser_info(parser_info_template='aiida-quantumespresso parser ph.x v{}')

        try:
            with open(filepath, 'r') as handle:
                output = handle.readlines()
        except IOError:
            raise QEOutputParsingError("failed to read file: {}.".format(filepath))

        # Empty output can be considered as a problem
        if not output:
            result['parser_warnings'].append('no_output')
            is_successful = False
            return is_successful, result

        # Parse the output line by line by creating an iterator of the lines
        it = iter(output)
        for line in it:

            # If the output does not contain the line with 'JOB DONE' the program was prematurely terminated
            if 'JOB DONE' in line:
                is_terminated = False

            # If the run did not convergence we expect to find the following string
            match = re.search('.*Convergence has not been reached after\s+([0-9]+)\s+iterations!.*', line)
            if match:
                result['parser_warnings'].append('not_converged')
                is_successful = False

        if is_terminated:
            result['parser_warnings'].append('terminated')
            is_successful = False
        return is_successful, result

    def parse_elphmat(self, filepath):
        """
        Parse the contents of the file elpmat as written by a EPCCalculation

        :param filepath: absolute filepath to the elpmat output file
        :returns: an array in the format: [[k,q,elphmat] for k,q in elphmat content]
        """

        try:
            with open(filepath, "r") as handle:
                data=handle.readlines()
        except IOError as exception:
            raise OutputParsingError("could not read the '{}' output file".format(os.path.basename(filepath)))

        for i,line in enumerate(data[0:10]):
            if ' q =' in line: qvec = np.array([float(x) for x in line[5:].split()])
            if ' nat =' in line: nat=int(line.split()[-1])
            if ' bands =' in line: bands=[int(x) for x in line.split()[2:]]
            if ' ibmin, ibmax =' in line:
                ibmin, ibmax = [int(x) for x in line.split()[-2:]]
                bands = range(ibmin, ibmax+1)

        calc = self.node
        # In SOC calculations with xml dynmats, for some reason, the qvec written out is NOT the rotated one...
        # we should use the input of the calculations
        qvec =  calc.inputs.qpoint.get_kpoints(cartesian = True)[0]
        cell = calc.inputs.qpoint.cell
        alat = np.linalg.norm(cell[0])
        twopioa = 2*np.pi/alat
        qvec = np.array(qvec)/twopioa
        # EPC is in cartesian coordinates. We project on phonon modes...
        # we first recover the dynamical matrix and diagonalize it
        if 'dynfile' in calc.inputs:
            dynname = calc.inputs.dynfile.list_object_names()[0]
            dynmat = calc.inputs.dynfile.get_object_content(dynname)
        else:
            dynname = calc.inputs.parent_folder.creator.outputs.retrieved.list_object_names('DYN_MAT')[0]
            dynmat = calc.inputs.parent_folder.creator.outputs.retrieved.get_object_content('DYN_MAT/'+dynname)
        isxml = "xml" in dynname
        D, w2star, zzstar = read_diag_calc_dynmat(dynmat, qvec, xml=isxml)
        # Now we read the rest of the elphmat files
        i=1
        kis = []
        elphs = []
        bnd_idxs = np.zeros((len(bands)**2, 2), dtype=int)
        while i<len(data):
            if 'elph at' in data[i]:
                kvec=[float(x) for x in data[i][20:].split()]
                # in some case I printed only the in-plane coord (2D mats)
                if len(kvec)<3: kvec.append(0.0)
                kis.append(kvec)
                i += 1
                ij = 0
                g2s_at_ki = np.zeros((len(bands)**2, 3*nat))
                while ij < len(bands)**2: # 4 because (ival, ival), (ival, icond)
                    if "elph mat" in data[i].strip():
                        bnd_idxs[ij] = np.array([int(x) for x in data[i].split()[-2:]])
                        begin=i+3
                        nmus = [begin + j * 3 * nat for j in range(3 * nat)]
                        elph_at_ki = np.array([[np.complex(float(l.split()[0]),float(l.split()[1]))
                                           for l in data[nmu:nmu+3*nat]] for nmu in nmus])
                        g2s_at_ki[ij] = proj_elph(np.abs(w2star),zzstar,elph_at_ki)
                        ij += 1
                        i += (3*nat)**2
                    else:
                        i += 1
                elphs.append(g2s_at_ki)
            else:
                i += 1
        bnd_idxs -= 1 # to go from Fortran indexing of the bands (starting at 1)
                      # to python indexing (starting at 0)

        out_dict = {'matrix_elements' : np.array(elphs), # indices: kis, ijs, in ev**2
                      'initial_states': np.array(kis)*twopioa,    # kis, in A-1...
                      'band_indices' : bnd_idxs,          # in aiida (python) indexing
                      'q_point' : qvec*twopioa,
                      'frequencies' : np.sign(w2star)*np.sqrt(np.abs(w2star))*RytoeV}                   # in in A-1

        return out_dict

    def parse_rotated_dvscf(self,dfilepath): #filepath):
        """
        Parse the contents of the Rotated_DVSCF folder created by an EPCCalculation

        :param filepath: absolute filepath to the rotated dvscf folder
        :returns: a dictionary with:
        the rotated q vectors
        the relative path to the rotated dvscf files
        the path to the folder
        """
        #dfilepath=os.path.join(filepath,
        #    EPCCalculation._PREFIX+"."+EPCCalculation._INPUT_DVSCF_SUFFIX+".dfile_dir")
        try:
            with open(dfilepath, "r") as handle:
                data=handle.readlines()
        except IOError as exception:
            raise OutputParsingError("could not read the '{}' output file".format(os.path.basename(dfilepath)))

        qs=[]
        dvscffiles=[]
        # For some reason sometimes (when run on a single processor??) the dfile_dir file is written differently:
        # either qcart, qlat, filename each line
        if len(data[0].split())>3:
            nqs=len(data)
            for i in range(nqs):
                info = data[i].split()
                qcart = [float(x) for x in info[:3]]
                file_sufix=info[-1]+"1"
                qs.append(qcart)
                dvscffiles.append("aiida."+file_sufix)
        # or qcart \n qlat \n filename \n on 3 lines
        else:
            nqs=int(len(data)/3)
            for i in range(nqs):
                qcart=[float(x) for x in data[3*i].split()]
                #qlat=[float(x) for x in data[3*i+1].split()]
                file_sufix=data[3*i+2].split()[-1]+"1"
                qs.append(qcart)
                dvscffiles.append("aiida."+file_sufix)
        return {'q_stars':qs, 'dvscf_files':dvscffiles}
