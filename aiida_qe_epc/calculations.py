# -*- coding: utf-8 -*-
import io
import os
import six
import numpy as np

from aiida import orm
from aiida.common import datastructures, exceptions
from aiida.common.lang import classproperty
from aiida.engine import CalcJob
from aiida.orm.utils import load_node

from aiida_quantumespresso.calculations import _lowercase_dict, _uppercase_dict
from aiida_quantumespresso.utils.convert import convert_input_to_namelist_entry

class EPCCalculation(CalcJob):
    """
    Quantum ESPRESSO EPC calculations
    """
    #
    _PREFIX = 'aiida'
    _INPUT_FILE_NAME = 'aiida.in'
    _OUTPUT_FILE_NAME = 'aiida.out'
    _OUTPUT_ELPHMAT_NAME = 'elphmat1' # USED TO BE ELPHMAT1
    _OUTPUT_STAR_FOLDER_NAME = 'Rotated_DVSCF'
    _INPUT_DVSCF_SUFFIX = 'dvscf'

    _default_parser = 'qe_epc'
    _compulsory_namelists = ['INPUTPH']

    # Keywords that cannot be set manually, only by the plugin
    _blocked_keywords = [
        ('INPUTPH', 'iverbosity'),
        ('INPUTPH', 'prefix'),
        ('INPUTPH', 'outdir'),
        ('INPUTPH', 'fildvscf'),
        ('INPUTPH', 'fildyn'),
        ('INPUTPH', 'trans'),
        ('INPUTPH', 'electron_phonon'),
    ]

    @classmethod
    def define(cls,spec):
        super(EPCCalculation, cls).define(spec)
        spec.input('metadata.options.input_filename', valid_type=six.string_types, default='aiida.in')
        spec.input('metadata.options.output_filename', valid_type=six.string_types, default='aiida.out')
        spec.input('metadata.options.parser_name', valid_type=six.string_types,
            default='qe_epc')
        spec.input('metadata.options.withmpi', valid_type=bool, default=True)
        spec.input('kpoints', valid_type=orm.KpointsData,
            help='Kpoints at which to compute EPC')
        spec.input('qpoint', valid_type=orm.KpointsData,
            help='qpoint of the phonon involved in EPC')
        spec.input('dynfile', valid_type=orm.SinglefileData, required=False,
            help='the dynamical matrix file')
        spec.input('dvscf', valid_type=orm.SinglefileData, required = False,
            help='the dvscf file')
        spec.input('parameters', valid_type=orm.Dict,
            help="{u'INPUTPH': {u'alpha_mix': 0.3, u'tr2_ph': 1e-18}}")
        spec.input('parent_folder', valid_type=(orm.RemoteData, orm.FolderData),
            help='the folder of a completed `PhCalculation`')
        spec.input('settings', valid_type=orm.Dict, required=False,
             help='mostly used by Star workchain')
        #
        spec.output('output_parameters', valid_type=orm.Dict)
        spec.output('Rotated_DVSCF', required=False, valid_type=orm.Dict)
        spec.output('elphmat', valid_type=orm.ArrayData)
        spec.default_output_node = 'output_parameters'
        spec.exit_code(
            100, 'ERROR_NO_RETRIEVED_FOLDER', message='The retrieved folder data node could not be accessed.')
        spec.exit_code(
            110, 'ERROR_READING_OUTPUT_FILE', message='The output file could not be read from the retrieved folder.')
        spec.exit_code(
            111, 'ERROR_READING_ELPHMAT_FILE', message='The elphmat file could not be read from the retrieved folder.')

    @classproperty
    def _OUTPUT_SUBFOLDER(cls):
        return './out/'

    @classproperty
    def _FOLDER_PH0(cls):
        return os.path.join(cls._OUTPUT_SUBFOLDER, '_ph0')

    @classproperty
    def _FOLDER_DYNAMICAL_MATRIX(cls):
        return 'DYN_MAT'
    @classproperty
    def _OUTPUT_DYNAMICAL_MATRIX_PREFIX(cls):
        return os.path.join(cls._FOLDER_DYNAMICAL_MATRIX,
                                                      'dynamical-matrix-')
    #
    def prepare_for_submission(self, folder):
        """Create the input files from the input nodes passed to this instance of the `CalcJob`.

        :param folder: an `aiida.common.folders.Folder` to temporarily write files on disk
        :return: `aiida.common.datastructures.CalcInfo` instance
        """
        if 'settings' in self.inputs:
            settings = self.inputs.settings.get_dict()
        else:
            settings = {}
        # First prepare and write the input files:
        qpoint = self.inputs.qpoint
        #
        # Note that q point needs to be in twopioa...
        if len(qpoint.get_kpoints())>1:
            raise exceptions.InputValidationError("single q only in input")
        cell = qpoint.cell
        alat = np.linalg.norm(cell[0])
        twopioa = 2*np.pi/alat
        qout = np.array(qpoint.get_kpoints(cartesian = True)[0])/twopioa
        postpend_text = u'{}  {}  {}  \n'.format(*qout)
        # same for the kpoints. I dare assume the cell is the same...
        kpoints = self.inputs.kpoints
        list_of_kpoints=[np.array(k)/twopioa for k in kpoints.get_kpoints(cartesian = True)]
        postpend_text += u'{}\n'.format(len(list_of_kpoints))
        for points in list_of_kpoints:
            postpend_text += u'{}  {}  {}  \n'.format(*points)
        #
        # Transform first-level keys (i.e. namelist and card names) to uppercase and second-level to lowercase
        input_parameters = _uppercase_dict(self.inputs.parameters.get_dict(), dict_name='parameters')
        input_parameters = {k: _lowercase_dict(v, dict_name=k) for k, v in six.iteritems(input_parameters)}
        #
        # Check that required namelists are present
        for namelist in self._compulsory_namelists:
            if not namelist in input_parameters:
                raise exceptions.InputValidationError("the required namelist '{}' was not defined").format(namelist)
        #
        # Check for presence of blocked keywords
        for namelist, flag in self._blocked_keywords:
            if namelist in input_parameters and flag in input_parameters[namelist]:
                raise InputValidationError("explicit definition of the '{}' "
                    "flag in the '{}' namelist or card is not allowed".format(flag, namelist))
        #
        #
        input_parameters['INPUTPH']['iverbosity'] = 1
        input_parameters['INPUTPH']['outdir'] = self._OUTPUT_SUBFOLDER
        input_parameters['INPUTPH']['prefix'] = self._PREFIX
        input_parameters['INPUTPH']['fildvscf'] = self._INPUT_DVSCF_SUFFIX
        input_parameters['INPUTPH']['fildyn'] = self._OUTPUT_DYNAMICAL_MATRIX_PREFIX
        input_parameters['INPUTPH']['trans'] = False
        input_parameters['INPUTPH']['electron_phonon'] = 'chosen_ks'
        #
        #
        input_filename = folder.get_abs_path(self._INPUT_FILE_NAME)
        with io.open(input_filename, 'w') as infile:
            for namelist_name in self._compulsory_namelists:
                namelist = input_parameters.pop(namelist_name,{})
                infile.write(u'&{0}\n'.format(namelist_name))
                for key, value in sorted(six.iteritems(namelist)):
                    infile.write(convert_input_to_namelist_entry(key, value))
                infile.write(u'/\n')
            #
            if postpend_text is not None:
                infile.write(postpend_text)

        if input_parameters:
            raise exceptions.InputValidationError('these specified namelists are invalid: {}'
                .format(', '.join(input_parameters.keys())))


        #tempfolder.get_subfolder(self._OUTPUT_SUBFOLDER, create=True)
        #tempfolder.get_subfolder(self._FOLDER_PH0, create=True)
        #folder.get_subfolder(self._FOLDER_DYNAMICAL_MATRIX,
        #                             create=True)

        #???
        #retrieve_list = self.get_retrieve_list(input_nodes)

        local_copy_list = []

        parent_folder = self.inputs.parent_folder
        if isinstance(parent_folder, orm.FolderData):
            folder_src = parent_folder.get_abs_path(self._OUTPUT_SUBFOLDER)
            folder_dst = self._OUTPUT_SUBFOLDER
            local_copy_list.append((parent_folder.uuid, folder_src, folder_dst))
        #
        if 'dvscf' in self.inputs:
            dvscffile = self.inputs.dvscf
            if isinstance(dvscffile, orm.SinglefileData):
                file_dst = os.path.join(self._FOLDER_PH0, self._PREFIX+"."+self._INPUT_DVSCF_SUFFIX+"1")
                local_copy_list.append((dvscffile.uuid, dvscffile.filename,file_dst))
        # else we use the one already in out/_ph0
        if 'dynfile' in self.inputs:
            # create a folder for the dynamical matrices
            folder.get_subfolder(self._FOLDER_DYNAMICAL_MATRIX, create=True)
            dynfile = self.inputs.dynfile
            if 'xml' in dynfile.filename:
                dest=self._OUTPUT_DYNAMICAL_MATRIX_PREFIX+".xml"
            else:
                dest=self._OUTPUT_DYNAMICAL_MATRIX_PREFIX
            local_copy_list.append((dynfile.uuid,dynfile.filename, dest))

        remote_copy_list = []
        parent_folder = self.inputs.parent_folder

        if isinstance(parent_folder, orm.RemoteData):
            remote_copy_list.append((
                parent_folder.computer.uuid,
                os.path.join(parent_folder.get_remote_path(), self._OUTPUT_SUBFOLDER),
                self._OUTPUT_SUBFOLDER
            ))
            if 'dynfile' not in self.inputs:
                folder_out = os.path.join(parent_folder.get_remote_path(),
                self._FOLDER_DYNAMICAL_MATRIX)
                remote_copy_list.append((
                parent_folder.computer.uuid,
                folder_out, '.'))

        if 'replace_dvscf' in settings.keys():
                folder_uuid, filename_ret = settings['replace_dvscf']
                folder_dvscf = load_node(folder_uuid)
                if isinstance(folder_dvscf, orm.FolderData):
                    file_dst = os.path.join(self._FOLDER_PH0, self._PREFIX+"."+self._INPUT_DVSCF_SUFFIX+"1")
                    retpath = os.path.join(self._OUTPUT_STAR_FOLDER_NAME,filename_ret)
                    local_copy_list.append((folder_uuid, retpath, file_dst))
                else:
                    # we copy it from the remote folder of the star calculation
                    source_path = os.path.join(folder_dvscf.get_remote_path(), self._OUTPUT_STAR_FOLDER_NAME,filename_ret)
                    dist_path =os.path.join(self._FOLDER_PH0, self._PREFIX+"."+self._INPUT_DVSCF_SUFFIX+"1")
                    remote_copy_list.append((folder_dvscf.computer.uuid, source_path, dist_path))
        # Empty command line by default

        codeinfo = datastructures.CodeInfo()
        codeinfo.cmdline_params = (list(settings.pop('CMDLINE', [])) + ['-in', self._INPUT_FILE_NAME])
        codeinfo.stdout_name = self._OUTPUT_FILE_NAME
        codeinfo.code_uuid = self.inputs.code.uuid

        calcinfo = datastructures.CalcInfo()
        calcinfo.uuid = str(self.uuid)
        calcinfo.codes_info = [codeinfo]
        calcinfo.local_copy_list = local_copy_list
        calcinfo.remote_copy_list = remote_copy_list
        calcinfo.remote_symlink_list = []

        calcinfo.retrieve_list = []
        calcinfo.retrieve_list.append(self._OUTPUT_FILE_NAME)
        calcinfo.retrieve_list.append(self._OUTPUT_ELPHMAT_NAME)
        if 'rotate_dvscf' in settings.keys():
            if settings['rotate_dvscf']==True:
                dfilepath=os.path.join(self._OUTPUT_STAR_FOLDER_NAME,
                            self._PREFIX+"."+self._INPUT_DVSCF_SUFFIX+".dfile_dir")
                calcinfo.retrieve_list.append(dfilepath)
        calcinfo.retrieve_list += settings.pop('ADDITIONAL_RETRIEVE_LIST', [])

        return calcinfo
