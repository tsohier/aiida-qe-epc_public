
# aiida-qe-epc_public

AiiDA plugin for EPC calculations with quantum espresso

This plugin is the default output of the
[AiiDA plugin cutter](https://github.com/aiidateam/aiida-plugin-cutter),
intended to help developers get started with their AiiDA plugins.

# Requirements
This package depends directly on `aiida-core >= v1.0.0` and the `aiida-quantumespresso` package.


## Features



## Installation

```shell
pip install -e .
verdi plugin list aiida.calculations  # should now show your calclulation plugins
```


## Usage

Here goes a complete example of how to submit a test calculation using this plugin.


## Development


## License

MIT


## Contact

thibault.sohier@gmail.com
